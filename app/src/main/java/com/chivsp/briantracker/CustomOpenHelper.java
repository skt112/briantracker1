package com.chivsp.briantracker;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;

public class CustomOpenHelper extends SQLiteOpenHelper {


    // データベースのバージョン(2,3と挙げていくとonUpgradeメソッドが実行される)
    static final private int VERSION = 1;

    private static final String PLAYER_NAME = "PLAYER_NAME TEXT";

    // コンストラクタ　以下のように呼ぶこと
    public CustomOpenHelper(Context context){
        super(context, "Briantracker_DB", null, VERSION);
    }

    // データベースが作成された時に実行される処理
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*
          テーブルを作成する
          execSQLメソッドにCREATET TABLE命令を文字列として渡すことで実行される
          引数で指定されているものの意味は以下の通り
          引数1 ・・・ id：列名 , INTEGER：数値型 , PRIMARY KEY：テーブル内の行で重複無し , AUTOINCREMENT：1から順番に振っていく
          引数2 ・・・ name：列名 , TEXT：文字列型
          引数3 ・・・ price：列名 , INTEGER：数値型
         */
        db.execSQL("CREATE TABLE PLAYER_INFO (" +
                "PLAYER_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PLAYER_NAME TEXT)");
        saveData(db,"music1");
    }

    public void saveData(SQLiteDatabase db, String PLAYER_NAME){
        ContentValues values = new ContentValues();
        values.put("PLAYER_NAME", PLAYER_NAME);

        db.insert("PLAYER_INFO", null, values);
    }
    // データベースをバージョンアップした時に実行される処理
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 処理を記述
    }

    // データベースが開かれた時に実行される処理
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}