package com.chivsp.briantracker;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.content.ContentValues;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomOpenHelper helper = new CustomOpenHelper(this);
        final SQLiteDatabase db = helper.getWritableDatabase();

        final EditText nameText = findViewById(R.id.editName);

        Button entryButton =  findViewById(R.id.CNext);
        entryButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameText.getText().toString();

                ContentValues insertValues = new ContentValues();
                insertValues.put("PLAYER_NAME TEXT", name);
                db.insert("PLAYER_INFO", null, insertValues);
            }
        });

    }
    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_main, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }
    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        if (menuItem.getItemId() == R.id.action_disp_player) {// インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(MainActivity.this, SubActivity.class);
            // Activity起動
            startActivity(intent);
        }else {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(MainActivity.this, MainActivity.class);
            // Activity起動
            startActivity(intent);
        }

        return true;
    }
}
