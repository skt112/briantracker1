package com.chivsp.briantracker;

public class Table {
    public static final int MAX_PAYER = 10;
    Player arrPlayer[] = new Player[MAX_PAYER];
    Status arrStatus[] = new Status[MAX_PAYER];

    /**
     * コンストラクタ
     * テーブルのコンストラクタ
     *
     */
    public Table(){

    }

    /**
     * プレイヤーを追加する。
     *
     * @param seatNum
     * @param playerName
     */
    public void addPlayer(int seatNum,String playerName){

        arrPlayer[seatNum-1] = new Player(seatNum,playerName);

    }

    /**
     * プレイヤーを削除する。
     *
     * @param seatNum
     */
    public void delPlayer(int seatNum){

        arrPlayer[seatNum-1] = null;

    }

    /**
     * プレイヤーを取得する。
     *
     * @param seatNum
     */
    public Player getPlayer(int seatNum){

        return arrPlayer[seatNum-1];

    }

    /**
     * プレイヤのアクションを設定する。
     *
     * @param seatNum
     */
    public void setStatus(int seatNum,Status status){

        arrStatus[seatNum-1] =  status;

    }

    /**
     * Nextの処理を実行する。
     *
     */
    public void doNext(){

        for(int i=0;i < MAX_PAYER;i++){

            if(arrPlayer[i] == null)continue;

            arrPlayer[i].next(arrStatus[i]);

        }

    }

    /**
     * CNextの処理を実行する。
     *
     */
    public void doCNext(){

        for(int i=0;i < MAX_PAYER;i++){

            if(arrPlayer[i] == null)continue;

            arrPlayer[i].cNext(arrStatus[i]);

        }

    }
}
