package com.chivsp.briantracker;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.TextView;
import android.database.Cursor;
import android.widget.LinearLayout;
import android.database.sqlite.SQLiteDatabase;

public class SubActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        setContentView(layout);

        CustomOpenHelper helper = new CustomOpenHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        // queryメソッドの実行例
        Cursor c = db.query("PLAYER_INFO", null, null,
                null, null, null, null);

        boolean mov = c.moveToFirst();
        while (mov) {
            TextView textView = new TextView(this);
            textView.setText(c.getString(1));
            mov = c.moveToNext();
            layout.addView(textView);
        }
        c.close();
        db.close();

    }

    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_second, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }
    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        //アクション登録画面
        if (menuItem.getItemId() == R.id.action_register_action) {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動
            startActivity(intent);
        //新規登録
        }else {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動 新規登録
            startActivity(intent);
        }
        return true;
    }
}
